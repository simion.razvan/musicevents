package razvan.sem3.project.config;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import razvan.sem3.project.security.AuthorizationFilter;
import razvan.sem3.project.security.CorsFilter;

import java.util.logging.Level;
import java.util.logging.Logger;


public class CustomApplicationConfig extends ResourceConfig
{

    public CustomApplicationConfig()
    {
        packages("razvan.sem3.project.resources"); // find all resource endpoint classes in this package
        register(new ApplicationBinder());
        register(new CorsFilter());
        register(CorsFilter.class);
        register(AuthorizationFilter.class);
        // log exchanged http messages
        register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME),
                Level.INFO, LoggingFeature.Verbosity.PAYLOAD_ANY, LoggingFeature.DEFAULT_MAX_ENTITY_SIZE));
    }

}
