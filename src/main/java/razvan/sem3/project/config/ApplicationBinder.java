package razvan.sem3.project.config;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import razvan.sem3.project.data.EventRepository;
import razvan.sem3.project.data.EventRepositoryJPA;
import razvan.sem3.project.data.UserRepository;
import razvan.sem3.project.data.UserRepositoryJPA;
import razvan.sem3.project.model.Event;
import razvan.sem3.project.service.EventManagement;
import razvan.sem3.project.service.UserManagement;
import razvan.sem3.project.util.KeyGenerator;
import razvan.sem3.project.util.SimpleKeyGenerator;

public class ApplicationBinder extends AbstractBinder {
    @Override
    protected void configure() {
        bind(UserManagement.class).to(UserManagement.class);
        bind(UserRepositoryJPA.class).to(UserRepository.class);
        bind(EventManagement.class).to(EventManagement.class);
        bind(EventRepositoryJPA.class).to(EventRepository.class);
        bind(SimpleKeyGenerator.class).to(KeyGenerator.class);
    }
}
