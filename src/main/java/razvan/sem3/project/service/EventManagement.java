package razvan.sem3.project.service;

import razvan.sem3.project.data.EventRepository;
import razvan.sem3.project.model.Event;
import razvan.sem3.project.model.Venue;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class EventManagement {

    //private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("userPU");
    //EntityManager em = emf.createEntityManager();

    public EntityManager em = PersistenceManager.getInstance().getEmf().createEntityManager();
    @Inject
    private EventRepository eventRepository;
    @PostConstruct
    private void init(){ eventRepository.setEm(em);
        eventRepository.setEm(em);
    }

    public List<Event> allEvents(){
        em.getTransaction().begin();
        List<Event> events = eventRepository.getEvents();
        em.getTransaction().commit();
        em.close();
        return events;
    }
    public List<Venue> allVenues(){return eventRepository.getVenues();}
    public boolean deleteEvent(Long id){return this.eventRepository.deleteEvent(id);}

    //Move this to other class when authorization is implemented
    //method for the clubOwner
    public Event createEvent(Event event){
        em.getTransaction().begin();
        Event event1 = eventRepository.saveEvent(event);
        em.getTransaction().commit();
        em.close();
        return event1;
    }
    public Event findEventById(Long id){return eventRepository.findEventById(id);}
    public Venue findVenueById(Long id){return eventRepository.findVenueById(id);}
    public Venue findVenueByName(String name){return eventRepository.findVenueByName(name);}
    public List<Venue> findVenueByOwnerId(Long ownerId){
        em.getTransaction().begin();
        List<Venue> venues = eventRepository.findVenueByOwnerId(ownerId);
        em.getTransaction().commit();
        em.close();
        return venues;
    }
    public List<Event> findEventsByVenueId(Long venueId){return eventRepository.findEventsByVenueId(venueId);}
    public List<Event> findEventByDate(String date){return eventRepository.findEventByDate(date);}
    //separate the business logic and data retrieving

}
