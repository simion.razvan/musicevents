package razvan.sem3.project.service;

import razvan.sem3.project.data.EventRepository;
import razvan.sem3.project.data.UserRepository;
import razvan.sem3.project.model.*;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class UserManagement {


    //private static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("userPU");
    //public EntityManager em =  emf.createEntityManager();
    public EntityManager em = PersistenceManager.getInstance().getEmf().createEntityManager();
    public UserManagement(){}
    //separate the business logic and data retrieving
    @Inject
    private UserRepository userRepository;
    @Inject
    private EventRepository eventRepository;
    @PostConstruct
    private void init(){ userRepository.setEm(em);
    eventRepository.setEm(em);
    }

    public List<User> allUsers(){
        return userRepository.getUsers();
    }

    public User getUserByUsername(String username){
        return userRepository.getUserByUsername(username);
    }

    public boolean deleteUser(User user){
        em.getTransaction().begin();
        boolean ok = userRepository.deleteUser(user);
        em.getTransaction().commit();
        em.close();
        return ok;
    }

    public List<User> allArtists(){
        return userRepository.getArtists();
    }

    public List<User> allOwners(){
        return userRepository.getOwners();
    }

    public List<Role>  allRoles(){return userRepository.getRoles();}

    public Role findRoleByName(String name){ return userRepository.getRoleByName(name);}
    public User findById(Long id){ return this.userRepository.findById(id);}

    public User create(User user){
        em.getTransaction().begin();
        User user1 = userRepository.save(user);
        em.getTransaction().commit();
        em.close();
        return user1;
    }

    //---------------------------------------------------------



}
