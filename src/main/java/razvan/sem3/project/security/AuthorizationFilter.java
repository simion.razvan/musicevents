package razvan.sem3.project.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import razvan.sem3.project.model.User;
import razvan.sem3.project.service.UserManagement;
import razvan.sem3.project.util.KeyGenerator;

import java.lang.reflect.Method;
import java.security.Key;
import java.util.*;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

public class AuthorizationFilter implements ContainerRequestFilter {

    /**
     * - resourceInfo contains information about the requested operation (GET, PUT,
     * POST …). - resourceInfo will be assigned/set automatically by the Jersey
     * framework, you do not need to assign/set it.
     */
    @Context
    private ResourceInfo resourceInfo;
    @Inject
    KeyGenerator keyGenerator;
    @Inject
    UserManagement userManagement;

    // requestContext contains information about the HTTP request message
    @Override
    public void filter(ContainerRequestContext requestContext) {

        // AUTHORIZATION:

        /*
         * Get information about the service method which is being called. This
         * information includes the annotated/permitted roles.
         */
        Method method = resourceInfo.getResourceMethod();
        // if access is allowed for all -> do not check anything further : access is
        // approved for all
        if (method.isAnnotationPresent(PermitAll.class)) {
            return;
        }

        // if access is denied for all: deny access
        if (method.isAnnotationPresent(DenyAll.class)) {
            Response response = Response.status(Response.Status.FORBIDDEN).build();
            requestContext.abortWith(response);
            return;
        }

        /*
         * here you do 1. the AUTHENTICATION first (as explained in previous sections),
         * and 2. if AUTHENTICATION succeeds, you do the authorization like this:
         */
        if (method.isAnnotationPresent(RolesAllowed.class)) {
            // get allowed roles for this method
            RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
            Set<String> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));

            /*
             * isUserAllowed : implement this method to check if this user has any of the
             * roles in the rolesSet if not isUserAllowed abort the requestContext with
             * FORBIDDEN response
             */
            if (!isUserAllowed(requestContext, rolesSet)) {
                Response response = Response.status(Response.Status.FORBIDDEN).build();
                requestContext.abortWith(response);
                return;
            }
        }

    }

    private boolean isUserAllowed(ContainerRequestContext requestContext, Set<String> rolesSet) {
        String token = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION).substring("Bearer".length()).trim();

        try{
            Key key = keyGenerator.generateKey();
            Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
            //a user can have multiple roles
            ArrayList<String> roles = ((ArrayList<String>) claims.getBody().get("role"));
            String username = claims.getBody().getSubject();
            User u = userManagement.getUserByUsername(username);
            if(u != null){
                //dont need to perform checking for the username
                //otherwise the u is null
                for (String role:roles) {
                    if (!rolesSet.contains(role)) return false;
                }
                return true;
            }
            //request username from the db
        }catch(Exception e){
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
        return false;

    }
}
