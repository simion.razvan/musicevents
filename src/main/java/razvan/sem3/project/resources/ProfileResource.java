package razvan.sem3.project.resources;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.*;

import java.security.Key;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import razvan.sem3.project.model.Role;
import razvan.sem3.project.model.User;
import razvan.sem3.project.service.UserManagement;
import razvan.sem3.project.util.KeyGenerator;

import static javax.ws.rs.core.HttpHeaders.AUTHORIZATION;

@Path("/profiles")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProfileResource {

    @Context
    private UriInfo uriInfo;

    @Inject
    private UserManagement userManagement;

    @Inject
    private KeyGenerator keyGenerator;

    private Set<Role> userRoles = null;
    private Set<String> role = new HashSet<>();
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response authenticateUser(UserDTO userDTO){
        try {
            String login = userDTO.getUsername();
            String password = userDTO.getPassword();

            if(userDTO.getUsername() == null || userDTO.getPassword() == null){
                return Response.status(400, "Not all fields have been entered.").build();
            }
            //validate username
            User authenticatedUser = authenticate(login);
            if(authenticatedUser == null){
                return Response.status(400, "No account with this username has been registered").build();
            }
            //TODO: change this when hashing passwords
            //validate passwords
            boolean isMatch = password.equals(authenticatedUser.getPassword());
            if(!isMatch){
                return Response.status(400, "Invalid credentials.").build();
            }

            String token = issueToken(login, role, authenticatedUser.getId());
            UserJwtLoginDTO userRes = new UserJwtLoginDTO(
                    token,
                    authenticatedUser.getId(),
                    authenticatedUser.getUsername(),
                    authenticatedUser.getFirstName(),
                    authenticatedUser.getLastName());


            //include the authenticated user in the response only
            //if more information than username and role(which are included in token) are needed
            return Response.ok(userRes).header(AUTHORIZATION, "Bearer " + token).build();

        }catch(Exception e){
            return Response.status(Response.Status.UNAUTHORIZED).entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/tokenIsValid")
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkTokenIsValid(ContainerRequestContext requestContext){
        try{
            Key key = keyGenerator.generateKey();

            String token = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION).substring("Bearer".length()).trim();

            if(token.equals("")){
                return Response.ok(false).build();
            }
            Jws<Claims> verified = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
            if(verified == null){
                return Response.ok(false).build();
            }
            String username = verified.getBody().getSubject();
            User verifyUser = authenticate(username);
            if(verifyUser == null){
                return Response.ok(false).build();
            }
            return Response.ok(true).build();
        }catch(Exception e){
            return Response.status(500, e.getMessage()).build();
        }
    }

    @PermitAll
    @GET
    @Path("/user")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAuthenticatedUserInfo(ContainerRequestContext requestContext){

        String token = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION).substring("Bearer".length()).trim();

        Key key = keyGenerator.generateKey();
        Jws<Claims> claims = Jwts.parser().setSigningKey(key).parseClaimsJws(token);

        String username = claims.getBody().getSubject();
        User u = userManagement.getUserByUsername(username);
        UserJwtLoginDTO userRes = new UserJwtLoginDTO(
                token,
                u.getId(),
                u.getUsername(),
                u.getFirstName(),
                u.getLastName());
        return Response.ok(userRes).build();
    }

    private User authenticate(String login) {
        //SHOULD I only check if the username and pass are correct?
        //OR SHOULD I get the list with all users and check if the user exists in the db
        User user = userManagement.getUserByUsername(login);
        if(user != null){
            this.userRoles = user.getRoles();
            if(this.userRoles != null){
                for (Role r: this.userRoles) {
                    this.role.add(r.getName());
                }
            }
        }
        return user;
    }

    //for demo investigate production ready way to issue token
    private String issueToken(String login, Set<String> role, Long id){
        Key key = keyGenerator.generateKey();
        return Jwts.builder()
                .setSubject(login)
                .claim("role", role) //get the role from the db
                .claim("id", id)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .setIssuedAt(new Date())
                //.setExpiration()
                .signWith(SignatureAlgorithm.HS512, key)
                .compact();
    }

}
