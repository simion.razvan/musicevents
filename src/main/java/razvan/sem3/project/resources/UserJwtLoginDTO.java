package razvan.sem3.project.resources;


public class UserJwtLoginDTO {
    private String token;
    private Long id;
    private String username;
    private String firstName;
    private String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserJwtLoginDTO(String token, Long id, String username, String fName, String lName){
        this.id = id;
        this.token = token;
        this.username = username;
        this.firstName = fName;
        this.lastName = lName;
    }
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
