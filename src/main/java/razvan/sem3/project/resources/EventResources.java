package razvan.sem3.project.resources;

import razvan.sem3.project.model.Event;
import razvan.sem3.project.model.Venue;
import razvan.sem3.project.service.EventManagement;
import razvan.sem3.project.service.UserManagement;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Path("/")
public class EventResources {

    @Context
    private UriInfo uriInfo;

    @Inject
    private EventManagement eventManagement;

    @PermitAll
    @GET
    @Path("/venues")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllVenues(){
        GenericEntity<List<Venue>> venues = new GenericEntity<List<Venue>>(eventManagement.allVenues()){};
        return Response.ok(venues).build();
    }

    @PermitAll
    @GET
    @Path("/events")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllEvents(){
        GenericEntity<List<Event>> events = new GenericEntity<List<Event>>(eventManagement.allEvents()){};
        return Response.ok(events).build();
    }

    @PermitAll
    @GET  //GET at https://localhost:9090/event/1
    @Path("/event/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    //search for an user by id
    public Response getEvent(@PathParam("id") Long id){
        //the rating column of an artist it s not retrieved
        Event event = eventManagement.findEventById(id);
        return Response.ok(event).build();
    }

    @RolesAllowed("OWNER")
    @POST
    @Path("/event")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createEvent(EventDTO eventDTO){
        //in the JSON is passed the venueId
        //use this to find the venue
        Venue v = eventManagement.findVenueByName(eventDTO.getVenueName());
        final Event event = eventDTO.convert2Event();
        //then assign to the event the venue
        event.setVenue(v);
        Event saved = eventManagement.createEvent(event);
        String url = uriInfo.getAbsolutePath() + "/" + saved.getId();
        URI uri = URI.create(url);
        return Response.created(uri).build();
    }

    @PermitAll
    @GET
    @Path("/events/{date}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventsByDate(@PathParam("date") String date){
        GenericEntity<List<Event>> events = new GenericEntity<List<Event>>(eventManagement.findEventByDate(date)){};
        return Response.ok(events).build();
    }
    @RolesAllowed({"OWNER", "ADMIN"})
    @GET
    @Path("/venues/{ownerId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVenuesByOwnerId(@PathParam("ownerId") Long ownerId){
        GenericEntity<List<Venue>> venues = new GenericEntity<List<Venue>>(eventManagement.findVenueByOwnerId(ownerId)){};
        return Response.ok(venues).build();
    }

    @PermitAll
    @GET
    @Path("/events/venue/{venueId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEventsByVenueId(@PathParam("venueId") Long venueId){
        GenericEntity<List<Event>> events = new GenericEntity<List<Event>>(eventManagement.findEventsByVenueId(venueId)) {};
        return Response.ok(events).build();
    }

    //improve this
    //not sure if it works
    //ONLY POSSIBLE TO BE MADE BY CLUB OWNER WHO OWNS THE EVENT
    @RolesAllowed({"OWNER", "ADMIN"})
    @DELETE
    @Path("/event/{id}")
    public Response deleteEventById(@PathParam("id") Long id){
        boolean message = eventManagement.deleteEvent(id);
        Map<String, Boolean> response = new HashMap<>();
        if(message)
        {
            response.put("deleted", Boolean.TRUE);
            return Response.ok(response).build();
        }
        response.put("event not found", Boolean.FALSE);
        return Response.ok(response).build();
    }
}
