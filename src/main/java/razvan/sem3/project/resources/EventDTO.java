package razvan.sem3.project.resources;

import razvan.sem3.project.model.Event;
import razvan.sem3.project.model.Venue;
import razvan.sem3.project.service.EventManagement;

import java.util.Date;

public class EventDTO {
    private Venue venue;
    private Long venueId;
    private String venueName;

    public String getVenueName() {
        return venueName;
    }

    public void setVenueName(String venueName) {
        this.venueName = venueName;
    }

    private String name;
    private String description;
    private String date;
    private String time;

    public EventDTO(){

    }
    public Long getVenueId() {
        return venueId;
    }

    public void setVenueId(Long venueId) {
        this.venueId = venueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Event convert2Event(){
        return new Event(name, venueId, description, date, time);
    }
}
