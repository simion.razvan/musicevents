package razvan.sem3.project.resources;

import razvan.sem3.project.model.*;
import razvan.sem3.project.service.UserManagement;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.print.attribute.standard.Media;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Path("/")
public class UserResources {

    @Context
    private UriInfo uriInfo;

    @Inject
    private UserManagement userManagement;

    @PermitAll
    @GET
    @Path("/artists")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllArtists(){
        GenericEntity<List<User>> artists = new GenericEntity<List<User>>(userManagement.allArtists()){};
        return Response.ok(artists).build();
    }

    //@RolesAllowed({"ARTIST", "ADMIN"})
    @PermitAll
    @GET
    @Path("/owners")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllOwners(){
        GenericEntity<List<User>> owners = new GenericEntity<List<User>>(userManagement.allOwners()){};
        return Response.ok(owners).build();
    }

    @PermitAll
    @GET  //GET at https://localhost:9090/1
    @Path("/user/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    //search for an user by id
    public Response getUser(@PathParam("id") Long id){
        User user = userManagement.findById(id);
        return Response.ok(user).build();
    }


    @PermitAll
    @POST
    @Path("/user/register")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createUserAccount(UserDTO userDTO){
        //check if this works
        final User user = userDTO.convert2User();
        Set<Role> roles = new HashSet<>();
        Role userRole = userManagement.findRoleByName("USER");
        roles.add(userRole);
        user.setRoles(roles);
        User saved = userManagement.create(user);
        String url = uriInfo.getAbsolutePath() + "/" + saved.getId();
        URI uri = URI.create(url);
        return Response.created(uri).build();
    }

    @PermitAll
    @PUT
    @Path("/user/{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUserEmailAndPass(@PathParam("id") Long id, UserDTO userDTO){
        User user = userManagement.findById(id);
        if(user == null){
            return Response.status(400, "No account with this id has been registered").build();
        }
        if(userDTO.getEmail().equals("")){
            user.setPassword(userDTO.getPassword());
            userManagement.create(user);
        }else if(userDTO.getPassword().equals("")){
            user.setEmail(userDTO.getEmail());
            userManagement.create(user);
        }else if(!userDTO.getEmail().equals("") && !userDTO.getPassword().equals("")){
            user.setEmail(userDTO.getEmail());
            user.setPassword(userDTO.getPassword());
            userManagement.create(user);
        }
        return Response.ok(user).build();
    }

    @PermitAll
    @PUT
    @Path("/user/{id}/update/password")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUserPassword(@PathParam("id") Long id, UserDTO userDTO){
        User user = userManagement.findById(id);
        if(user == null){
            return Response.status(400, "No account with this id has been registered").build();
        }
        if(user.getPassword() == userDTO.getPassword()){
            return Response.status(400, "Not the correct password").build();
        }
        user.setPassword(userDTO.getUpdatePassword());
        userManagement.create(user);
        return Response.ok(user).build();
    }

}
