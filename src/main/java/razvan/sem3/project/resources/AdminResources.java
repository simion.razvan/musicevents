package razvan.sem3.project.resources;

import razvan.sem3.project.model.Role;
import razvan.sem3.project.model.User;
import razvan.sem3.project.service.UserManagement;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Path("/admin")
public class AdminResources {
    @Context
    private UriInfo uriInfo;
    @Inject
    private UserManagement userManagement;

    @RolesAllowed("ADMIN")
    @GET
    @Path("/users")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllUsers(){
        GenericEntity<List<User>> users = new GenericEntity<List<User>>(userManagement.allUsers()){};
        return Response.ok(users).build();
    }
    @RolesAllowed("ADMIN")
    @GET
    @Path("/users/async")
    @Produces(MediaType.APPLICATION_JSON)
    public void getAllUsersAsync(@Suspended final AsyncResponse asyncResponse){
        try{
            Thread.sleep(1000);
        }catch(Exception e) {

        }
        new Thread(new Runnable(){
            @Override
            public void run(){
                GenericEntity<List<User>> users = new GenericEntity<List<User>>(userManagement.allUsers()){};
                Response response = Response.ok(users).build();
                asyncResponse.resume(response);
            }
        }).start();
    }

    @RolesAllowed("ADMIN")
    @DELETE
    @Path("/async/delete/user/{id}")
    public void deleteUserById(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id){
        try{
            Thread.sleep(1000);
        }catch(Exception e) {

        }
        new Thread(new Runnable(){
            @Override
            public void run(){
                boolean ok = false;
                User u = userManagement.findById(id);
                if(u != null){
                    ok = userManagement.deleteUser(u);
                }
                else{
                    asyncResponse.resume(Response.status(400, "No account with this id has been registered").build());
                }
                if(ok == true){
                    asyncResponse.resume(Response.status(200, "User has been successfully deleted.").build());
                }
                asyncResponse.resume(Response.status(400, "User has been unsuccessfully deleted.").build());
            }
        }).start();

    }

    @PermitAll
    @PUT
    @Path("/async/user/update/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void updateUserAsync(@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long id, UserDTO userDTO){
        try{
            Thread.sleep(1000);
        }catch(Exception e) {

        }
        new Thread(new Runnable(){
            @Override
            public void run(){
                User user = userManagement.findById(id);
                if(user == null){
                    Response response = Response.status(400, "No account with this id has been registered").build();
                    asyncResponse.resume(response);
                }
                user.setFirstName(userDTO.getFirstName());
                user.setLastName(userDTO.getLastName());
                user.setEmail(userDTO.getEmail());

                //SET USER ROLES
                Set<String> rolesStr = userDTO.getRoles();
                Set<Role> roles = new HashSet<>();
                if(rolesStr != null){
                    for (String r: rolesStr) {
                        if(r.equals("ADMIN")){
                            Role adminRole = userManagement.findRoleByName("ADMIN");
                            if(adminRole != null){
                                roles.add(adminRole);
                            }
                        }
                        if(r.equals("OWNER")){
                            Role ownerRole = userManagement.findRoleByName("OWNER");
                            if(ownerRole != null){
                                roles.add(ownerRole);
                            }
                        }
                        if(r.equals("USER")){
                            Role userRole = userManagement.findRoleByName("USER");
                            if(userRole != null){
                                roles.add(userRole);
                            }
                        }
                        if(r.equals("ARTIST")){
                            Role artistRole = userManagement.findRoleByName("ARTIST");
                            if(artistRole != null){
                                roles.add(artistRole);
                            }
                        }
                    }
                }
                user.setRoles(roles);

                userManagement.create(user);
                Response response = Response.status(200, "user updated successfully").build();
                asyncResponse.resume(response);
            }
        }).start();
    }
    @PermitAll
    @PUT
    @Path("/user/update/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("id") Long id, UserDTO userDTO){
        User user = userManagement.findById(id);
        if(user == null){
            return Response.status(400, "No account with this id has been registered").build();
        }
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());

        //SET USER ROLES
        Set<String> rolesStr = userDTO.getRoles();
        Set<Role> roles = new HashSet<>();
        if(rolesStr != null){
            for (String r: rolesStr) {
                if(r.equals("ADMIN")){
                    Role adminRole = userManagement.findRoleByName("ADMIN");
                    if(adminRole != null){
                        roles.add(adminRole);
                    }
                }
                if(r.equals("OWNER")){
                    Role ownerRole = userManagement.findRoleByName("OWNER");
                    if(ownerRole != null){
                        roles.add(ownerRole);
                    }
                }
                if(r.equals("USER")){
                    Role userRole = userManagement.findRoleByName("USER");
                    if(userRole != null){
                        roles.add(userRole);
                    }
                }
                if(r.equals("ARTIST")){
                    Role artistRole = userManagement.findRoleByName("ARTIST");
                    if(artistRole != null){
                        roles.add(artistRole);
                    }
                }
            }
        }
        user.setRoles(roles);

        userManagement.create(user);
        return Response.status(200, "user updated successfully").build();
    }


}
