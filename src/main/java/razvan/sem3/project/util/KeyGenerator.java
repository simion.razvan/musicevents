package razvan.sem3.project.util;

import java.security.Key;

public interface KeyGenerator {
    Key generateKey();
}
