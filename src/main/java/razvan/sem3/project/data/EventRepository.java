package razvan.sem3.project.data;

import razvan.sem3.project.model.Event;
import razvan.sem3.project.model.Venue;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public interface EventRepository {
    void setEm(EntityManager em);
    List<Event> getEvents();
    List<Venue> getVenues();
    Event saveEvent(Event e);
    Event findEventById(Long id);
    boolean deleteEvent(Long id);
    Venue findVenueById(Long id);
    Venue findVenueByName(String name);
    List<Event> findEventByDate(String date);
    List<Event> findEventsByVenueId(Long id);
    List<Venue> findVenueByOwnerId(Long ownerId);
}
