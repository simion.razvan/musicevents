package razvan.sem3.project.data;

import org.hibernate.jpa.QueryHints;
import razvan.sem3.project.model.*;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepositoryJPA implements UserRepository{

    private EntityManager em;
    public void setEm(EntityManager em){
        this.em = em;
    }

    @Override
    public List<User> getUsers() {
        return em.createQuery("from User", User.class).getResultList();
    }

    @Override
    public List<User> getArtists() {
        return em.createQuery("select distinct u from User u join fetch u.roles rl where rl.id = :id", User.class)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .setParameter("id", 4L)
                .getResultList();
    }


    @Override
    public User getUserByUsername(String username) {
        try{
            return em.createQuery("select u from User u" +
                    " where u.username = :username", User.class)
                    .setParameter("username", username)
                    .getSingleResult();
        }catch(Exception e){}
        return null;
    }

    @Override
    public List<Role> getRoles() {
        return null;
    }

    @Override
    public Role getRoleByName(String name) {
        return em.createQuery("select r from Role r where r.name = :name", Role.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public boolean deleteUser(User u) {
        try{
            em.remove(u);
        }catch(Exception e){
            return false;
        }
        return true;
    }

    @Override
    public List<User> getOwners() {
        return em.createQuery("select distinct u from User u join fetch u.roles rl where rl.id = :id", User.class)
                .setHint(QueryHints.HINT_PASS_DISTINCT_THROUGH, false)
                .setParameter("id", 3L)
                .getResultList();
    }

    @Override
    public User findById(Long id) {
        return em.find(User.class, id);
    }

    @Override
    public User save(User user) {
        em.persist(user);
        return user;
    }



}
