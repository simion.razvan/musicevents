package razvan.sem3.project.data;

import razvan.sem3.project.model.*;

import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import java.util.List;

public interface UserRepository {

    void setEm(EntityManager em);
    List<User> getUsers();
    User findById(Long id);
    User save(User user);
    List<User> getArtists();
    List<User> getOwners();
    User getUserByUsername(String username);
    boolean deleteUser(User u);
    List<Role> getRoles();
    Role getRoleByName(String name);
}
