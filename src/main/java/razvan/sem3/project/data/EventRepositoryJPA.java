package razvan.sem3.project.data;


import org.hibernate.jpa.QueryHints;
import razvan.sem3.project.model.Event;
import razvan.sem3.project.model.Venue;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class EventRepositoryJPA implements EventRepository {

    private EntityManager em;
    public void setEm(EntityManager em){
        this.em = em;
    }
    @Override
    public List<Event> getEvents() {
        return em.createQuery("from Event", Event.class).getResultList();
    }

    @Override
    public List<Venue> getVenues() {
        return em.createQuery("from Venue", Venue.class).getResultList();
    }

    @Override
    public Venue findVenueById(Long id) {
        return em.find(Venue.class, id);
    }

    @Override
    public Event saveEvent(Event e) {
        em.persist(e);
        return e;
    }
    @Override
    public Event findEventById(Long id){
        return em.find(Event.class, id);
    }

    @Override
    public Venue findVenueByName(String name) {
        try{
            return em.createQuery("select v from Venue  v where v.name = :name", Venue.class)
                    .setParameter("name", name)
                    .getSingleResult();
        }catch(Exception e){}
        return null;
    }

    //not working
    @Override
    public List<Venue> findVenueByOwnerId(Long id) {
        return em.createQuery("select v from Venue v join fetch v.user_id u where u.id = :id", Venue.class)
                .setParameter("id", id)
                .getResultList();
    }

    @Override
    public List<Event> findEventsByVenueId(Long id) {
        return em.createQuery("select e from Event e where e.venue.id = :id", Event.class)
                .setParameter("id", id)
                .getResultList();
    }

    @Override
    public boolean deleteEvent(Long id){
        Event event = null;
        try{
            event = em.find(Event.class, id);
        }catch(Exception e){
            return false;
        }
        em.remove(event);
        return true;
    }

    @Override
    public List<Event> findEventByDate(String date) {
        try{
            return em.createQuery("select ev from Event ev" +
                    " where ev.date = :date", Event.class)
                    .setParameter("date", date)
                    .getResultList();
        }catch(Exception e){}
        return null;
    }
}
