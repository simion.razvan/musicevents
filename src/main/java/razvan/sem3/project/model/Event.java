package razvan.sem3.project.model;

import javax.persistence.*;
import java.text.DateFormat;
import java.util.Date;


@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
    @OneToOne
    private Venue venue;
    private String name;
    private String description;
    private String date;
    private String time;
    @Transient
    Long venueId;




    public Event(String name, Long id, String description, String date, String time) {
        this.name = name;
        this.venueId = id;
        this.description = description;
        this.date = date;
        this.time = time;
    }

    public Event() {

    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Venue getVenue(){ return this.venue;}
    public void setVenue(Venue v){this.venue = v;}
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }

}
