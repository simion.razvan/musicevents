package razvan.sem3.project;

import org.glassfish.grizzly.http.HttpHeader;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.grizzly.websockets.WebSocketAddOn;
import org.glassfish.grizzly.websockets.WebSocketApplication;
import org.glassfish.grizzly.websockets.WebSocketEngine;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.grizzly.http.server.HttpServer;
import razvan.sem3.project.chat.ChatApplication;
import razvan.sem3.project.config.CustomApplicationConfig;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class deploys CustomApplicationConfig on a Grizzly server
 */
public class Publisher {

    private static final URI BASE_URI = UriBuilder.fromUri("http://0.0.0.0:/").port(9090).build();

    public static void main(String[] args) {

        try {
            CustomApplicationConfig customApplicationConfig = new CustomApplicationConfig();
            // create and start a grizzly server
            HttpServer server = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, customApplicationConfig, false);

            //setup static file handler so that we can also serve html pages
            StaticHttpHandler staticHandler = new StaticHttpHandler("static");
            staticHandler.setFileCacheEnabled(false);
            server.getServerConfiguration().addHttpHandler(staticHandler, "/static/");

            // Create WebSocket addon
            WebSocketAddOn addon = new WebSocketAddOn();
            for(NetworkListener listener : server.getListeners()){
                listener.registerAddOn(addon);
            }

            //register my websocket app
            WebSocketApplication chatApplication = new ChatApplication();
            WebSocketEngine.getEngine().register("/ws", "/chat",chatApplication);

            //Now start the server
            server.start();
            System.out.println("Hosting resources at " + BASE_URI.toURL());

        } catch (IOException ex) {
            Logger.getLogger(Publisher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}