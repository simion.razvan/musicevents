package razvan.sem3.project.resources;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import razvan.sem3.project.model.Event;
import razvan.sem3.project.model.Venue;

import static org.junit.jupiter.api.Assertions.*;

class EventDTOTest {

    private Venue venue1;
    private Event event1;
    private EventDTO eventDTO;

    @BeforeEach
    void setUp(){
        venue1 = new Venue("Shelter", "Frederiklaan", 150);
        venue1.setId(2L);
        event1 = new Event("Party", 2l, "Best Party", "2021-01-15", "22:00");
        eventDTO = new EventDTO();
        eventDTO.setDate("2021-01-15");
        eventDTO.setName("Party");
        eventDTO.setVenueId(2L);
        eventDTO.setDescription("Best Party");
        eventDTO.setTime("22:00");
    }
    @Test
    void EventDTO(){
        EventDTO event = new EventDTO();
        assertNotNull(event);
    }
    @Test
    void getVenueId() {
        assertEquals(venue1.getId(), eventDTO.getVenueId());
    }

    @Test
    void setVenueId() {
        eventDTO.setVenueId(3l);
        assertEquals(3l, eventDTO.getVenueId());
    }

    @Test
    void getName() {
        assertEquals("Party", eventDTO.getName());
    }

    @Test
    void setName() {
        eventDTO.setName("Crowder");
        assertEquals("Crowder", eventDTO.getName());
    }

    @Test
    void getDescription() {
        assertEquals("Best Party", eventDTO.getDescription());
    }

    @Test
    void setDescription() {
        eventDTO.setDescription("New party");
        assertEquals("New party", eventDTO.getDescription());
    }

    @Test
    void getDate() {
        assertEquals("2021-01-15", eventDTO.getDate());
    }

    @Test
    void setDate() {
        eventDTO.setDate("2021-01-17");
        assertEquals("2021-01-17", eventDTO.getDate());
    }

    @Test
    void getTime() {
        assertEquals("22:00", eventDTO.getTime());
    }

    @Test
    void setTime() {
        eventDTO.setTime("21:00");
        assertEquals("21:00", eventDTO.getTime());
    }

    @Test
    void convert2Event() {
        assertTrue(eventDTO.convert2Event() instanceof Event);
    }
}