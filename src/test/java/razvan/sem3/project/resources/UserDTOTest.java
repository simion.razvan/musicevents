package razvan.sem3.project.resources;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import razvan.sem3.project.model.Role;
import razvan.sem3.project.model.User;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class UserDTOTest {

    private User user1;

    private UserDTO userDTO;

    private Role role1;
    private Role role2;
    private Set<Role> roles = new HashSet<>();
    private Set<String> rolesStr = new HashSet<>();
    private Set<String> rolesStr2 = new HashSet<>();
    @BeforeEach
    void setUp(){
        user1 = new User("razvan1", "1234", "simion@gmail.com", "Razvan", "Simion");
        userDTO = new UserDTO();
        userDTO.setFirstName("Razvan");
        userDTO.setLastName("Simion");
        userDTO.setEmail("simion@gmail.com");
        userDTO.setUsername("razvan1");
        userDTO.setPassword("1234");
        userDTO.setUpdatePassword("razvan123");
        rolesStr.add("ADMIN");
        rolesStr.add("ARTIST");
        rolesStr2.add("USER");
        rolesStr2.add("OWNER");
        userDTO.setRoles(rolesStr);
    }
    @Test
    void UserDTO(){
        UserDTO user = new UserDTO();
        assertNotNull(user);
    }
    @Test
    void getRoles() {
        assertEquals(rolesStr, userDTO.getRoles());
    }

    @Test
    void setRoles() {
        userDTO.setRoles(rolesStr2);
        assertEquals(rolesStr2, userDTO.getRoles());
    }

    @Test
    void getEmail() {
        assertEquals("simion@gmail.com", userDTO.getEmail());
    }

    @Test
    void setEmail() {
        userDTO.setEmail("razvan@gmail.com");
        assertEquals("razvan@gmail.com", userDTO.getEmail());
    }

    @Test
    void getUpdatePassword() {
        assertEquals("razvan123", userDTO.getUpdatePassword());
    }

    @Test
    void setUpdatePassword() {
        userDTO.setUpdatePassword("123");
        assertEquals("123", userDTO.getUpdatePassword());
    }

    @Test
    void getUsername() {
        assertEquals("razvan1", userDTO.getUsername());
    }

    @Test
    void setUsername() {
        userDTO.setUsername("simion1");
        assertEquals("simion1", userDTO.getUsername());
    }

    @Test
    void getPassword() {
        assertEquals("1234", userDTO.getPassword());
    }

    @Test
    void setPassword() {
        userDTO.setPassword("12345");
        assertEquals("12345", userDTO.getPassword());
    }

    @Test
    void getFirstName() {
        assertEquals("Razvan", userDTO.getFirstName());
    }

    @Test
    void setFirstName() {
        userDTO.setFirstName("Michael");
        assertEquals("Michael", userDTO.getFirstName());
    }

    @Test
    void getLastName() {
        assertEquals("Simion", userDTO.getLastName());
    }

    @Test
    void setLastName() {
        userDTO.setLastName("Jackson");
        assertEquals("Jackson", userDTO.getLastName());
    }

    @Test
    void convert2User() {
        assertEquals(user1, userDTO.convert2User());
    }
}