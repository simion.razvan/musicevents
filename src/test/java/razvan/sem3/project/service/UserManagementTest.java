package razvan.sem3.project.service;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import razvan.sem3.project.data.UserRepository;
import razvan.sem3.project.model.Role;
import razvan.sem3.project.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;



@ExtendWith(MockitoExtension.class)
class UserManagementTest {

    @Mock
    private EntityManager em;
    @Mock
    private UserRepository userRepository;
    @Mock
    private EntityTransaction transaction;
    @InjectMocks
    private UserManagement userManagement;
    @BeforeEach
    public void init(){
        lenient().when(userManagement.em.getTransaction()).thenReturn(transaction);}

    @Test
    void When_getUsersIsCalled_Expect_ReturnAllUsers(){
        Mockito.when(userRepository.getUsers()).thenReturn(Stream.of(
                new User("razvan1", "1234", "simion@gmail.com", "Razvan", "Simion"),
                new User("alex1", "1234", "alex@gmail.com", "Alex", "Aciob"),
                new User("mike1", "1234", "mike@gmail.com", "Mike", "Parker"),
                new User("max1", "1234", "max1@gmail.com", "Max", "Johnson")
                ).collect(Collectors.toList())
        );
        Assertions.assertEquals(4, userManagement.allUsers().size());
        Mockito.verify(userRepository, Mockito.timeout(1)).getUsers();
    }

    @Test
    void When_getUserByUsernameIsCalled_Expect_ReturnUser(){
        User user = new User("razvan1", "1234", "simion@gmail.com", "Razvan", "Simion");
        userManagement.getUserByUsername("razvan1");
        Mockito.verify(userRepository, Mockito.timeout(1)).getUserByUsername(user.getUsername());
    }

    @Test
    void When_deleteUserIsCalled_Expect_DeleteUser(){
        User user = new User("razvan1", "1234", "simion@gmail.com", "Razvan", "Simion");
        userManagement.deleteUser(user);
        Mockito.verify(userRepository, Mockito.timeout(1)).deleteUser(user);
    }

    @Test
    void When_findRoleByNameIsCalled_Expect_ReturnRole(){
        Role role = new Role("ADMIN");
        userManagement.findRoleByName("ADMIN");
        Mockito.verify(userRepository, Mockito.timeout(1)).getRoleByName(role.getName());
    }
    @Test
    void When_findByIdIsCalled_Expect_ReturnUser(){
        User user = new User("razvan1", "1234", "simion@gmail.com", "Razvan", "Simion");
        user.setId(1L);
        userManagement.findById(user.getId());
        Mockito.verify(userRepository, Mockito.timeout(1)).findById(user.getId());
    }

    @Test
    void When_createIsCalled_Expect_UpdateUserOrCreateNewUser(){
        User user = new User("razvan1", "1234", "simion@gmail.com", "Razvan", "Simion");
        userManagement.create(user);
        Mockito.verify(userRepository, Mockito.timeout(1)).save(user);
    }
}