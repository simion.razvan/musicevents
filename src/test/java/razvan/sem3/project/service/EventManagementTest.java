package razvan.sem3.project.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import razvan.sem3.project.data.EventRepository;
import razvan.sem3.project.model.Event;
import razvan.sem3.project.model.User;
import razvan.sem3.project.model.Venue;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class EventManagementTest {

    @Mock
    private EntityManager em;
    @Mock
    private EventRepository eventRepository;
    @Mock
    private EntityTransaction transaction;
    @InjectMocks
    private EventManagement eventManagement;
    @BeforeEach
    public void init(){
        lenient().when(eventManagement.em.getTransaction()).thenReturn(transaction);
    }

    @Test
    void When_allEventsIsCalled_Expect_ReturnAllEvents(){
        Mockito.when(eventRepository.getEvents()).thenReturn(Stream.of(
                new Event("Party", 2l, "Best Party", "2021-01-15", "22:00:00"),
                new Event("New Party", 2l, "New Party", "2021-01-16", "22:00:00"),
                new Event("Test", 2l, "Test Party", "2021-01-17", "22:00:00")
                ).collect(Collectors.toList())
        );
        Assertions.assertEquals(3, eventManagement.allEvents().size());
        Mockito.verify(eventRepository, Mockito.timeout(1)).getEvents();
    }

    @Test
    void When_allVenuesIsCalled_Expect_ReturnAllVenues(){
        Mockito.when(eventRepository.getVenues()).thenReturn(Stream.of(
                new Venue("Shelter", "Frederiklaan", 150),
                new Venue("Upstairs", "Eindhoven", 150),
                new Venue("GH", "Amsterdam", 150)
                ).collect(Collectors.toList())
        );
        Assertions.assertEquals(3, eventManagement.allVenues().size());
        Mockito.verify(eventRepository, Mockito.timeout(1)).getVenues();
    }

    @Test
    void When_deleteUserIsCalled_Expect_DeleteUser(){
        Event event = new Event("Party", 2l, "Best Party", "2021-01-15", "22:00:00");
        event.setId(1L);
        eventManagement.deleteEvent(event.getId());
        Mockito.verify(eventRepository, Mockito.timeout(1)).deleteEvent(event.getId());
    }

    @Test
    void When_createIsCalled_Expect_UpdateEventOrCreateNewEvent(){
        Event event = new Event("Party", 2l, "Best Party", "2021-01-15", "22:00:00");
        eventManagement.createEvent(event);
        Mockito.verify(eventRepository, Mockito.timeout(1)).saveEvent(event);
    }

    @Test
    void When_findEventByIdIsCalled_Expect_ReturnEvent(){
        Event event = new Event("Party", 2l, "Best Party", "2021-01-15", "22:00:00");
        event.setId(1L);
        eventManagement.findEventById(event.getId());
        Mockito.verify(eventRepository, Mockito.timeout(1)).findEventById(event.getId());
    }

    @Test
    void When_findVenueByNameIsCalled_Expect_ReturnVenue(){
        Venue venue = new Venue("Shelter", "Frederiklaan", 150);
        eventManagement.findVenueByName(venue.getName());
        Mockito.verify(eventRepository, Mockito.timeout(1)).findVenueByName(venue.getName());
    }

    @Test
    void When_findEventsByVenueIdIsCalled_Expect_ReturnEvents(){
        Venue venue = new Venue("Shelter", "Frederiklaan", 150);
        venue.setId(2L);
        Mockito.when(eventRepository.findEventsByVenueId(venue.getId())).thenReturn(Stream.of(
                new Event("Party", 2l, "Best Party", "2021-01-15", "22:00:00"),
                new Event("New Party", 2l, "New Party", "2021-01-16", "22:00:00"),
                new Event("Test", 2l, "Test Party", "2021-01-17", "22:00:00")
                ).collect(Collectors.toList())
        );
        Assertions.assertEquals(3, eventManagement.findEventsByVenueId(venue.getId()).size());
        Mockito.verify(eventRepository, Mockito.timeout(1)).findEventsByVenueId(venue.getId());
    }

    @Test
    void When_findEventsByDateIsCalled_Expect_ReturnEvents(){
        Mockito.when(eventRepository.findEventByDate("2021-01-15")).thenReturn(Stream.of(
                new Event("Party", 2l, "Best Party", "2021-01-15", "22:00:00"),
                new Event("New Party", 2l, "New Party", "2021-01-15", "22:00:00"),
                new Event("Test", 2l, "Test Party", "2021-01-15", "22:00:00")
                ).collect(Collectors.toList())
        );
        Assertions.assertEquals(3, eventManagement.findEventByDate("2021-01-15").size());
        Mockito.verify(eventRepository, Mockito.timeout(1)).findEventByDate("2021-01-15");
    }

}