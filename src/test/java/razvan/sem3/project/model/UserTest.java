package razvan.sem3.project.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    private User user1;
    private Role role1;
    private Role role2;
    private Set<Role> roles = new HashSet<>();
    private String username = "user1";
    private String password = "1234";
    private String email = "user@gmail.com";
    private String firstName = "Razvan";
    private String lastName = "Simion";

    @BeforeEach
    void setUp(){
        user1 = new User("user1", "1234", "user1@gmail.com", "Razvan", "Simion");
        user1.setRating(5);
        user1.setId(1l);
        role1 = new Role("ADMIN");
        role2 = new Role("OWNER");
        roles.add(role1);
        roles.add(role2);
        user1.setRoles(roles);
    }

    @Test
    void User(){
        User user = new User();
        assertNotNull(user);
    }

    @Test
    void getRating() {
        assertEquals(5, user1.getRating());
    }

    @Test
    void setRating() {
        user1.setRating(4);
        assertEquals(4, user1.getRating());
    }

    @Test
    void getId() {
        assertEquals(1l, user1.getId());
    }

    @Test
    void setId() {
        user1.setId(2l);
        assertEquals(2l, user1.getId());
    }

    @Test
    void getUsername() {
        assertEquals("user1", user1.getUsername());
    }

    @Test
    void setUsername() {
        user1.setUsername("NewUser1");
        assertEquals("NewUser1", user1.getUsername());
    }

    @Test
    void getPassword() {
        assertEquals("1234", user1.getPassword());
    }

    @Test
    void setPassword() {
        user1.setPassword("NewPass1234");
        assertEquals("NewPass1234", user1.getPassword());
    }

    @Test
    void getEmail() {
        assertEquals("user1@gmail.com", user1.getEmail());
    }

    @Test
    void setEmail() {
        user1.setEmail("newEmail@gmail.com");
        assertEquals("newEmail@gmail.com", user1.getEmail());
    }

    @Test
    void getFirstName() {
        assertEquals("Razvan", user1.getFirstName());
    }

    @Test
    void setFirstName() {
        user1.setFirstName("Alex");
        assertEquals("Alex", user1.getFirstName());
    }

    @Test
    void getLastName() {
        assertEquals("Simion", user1.getLastName());
    }

    @Test
    void setLastName() {
        user1.setLastName("Aciob");
        assertEquals("Aciob", user1.getLastName());
    }

    @Test
    void getRoles() {
        assertEquals(roles, user1.getRoles());
    }

    @Test
    void setRoles() {
        Role role3 = new Role("ARTIST");
        Set<Role> roles1 = new HashSet<>();
        roles1.add(role3);
        user1.setRoles(roles1);
        assertEquals(roles1, user1.getRoles());
    }
}