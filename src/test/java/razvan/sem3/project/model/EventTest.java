package razvan.sem3.project.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import razvan.sem3.project.resources.UserDTO;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class EventTest {

    private Event event1;
    private User user1 = new User("user1", "1234", "user1@gmail.com", "Razvan", "Simion");
    private Venue venue1 = new Venue("Shelter", "Frederiklaan", 150);
    private Venue venue2 = new Venue("Guesthouse", "Popa Nan", 350);

    @BeforeEach
    void setUp(){
        event1 = new Event("Party", 2l, "Best Party", "2021-01-15", "22:00:00");
        event1.setId(1l);
        event1.setVenue(venue1);
    }
    @Test
    void Event(){
        Event event = new Event();
        assertNotNull(event);
    }
    @Test
    void getName() {
        assertEquals("Party", event1.getName());
    }

    @Test
    void setName() {
        event1.setName("New Party");
        assertEquals("New Party", event1.getName());
    }

    @Test
    void getVenue() {
        assertEquals(venue1, event1.getVenue());
    }

    @Test
    void setVenue() {
        event1.setVenue(venue2);
        assertEquals(venue2, event1.getVenue());
    }

    @Test
    void getDescription() {
        assertEquals("Best Party", event1.getDescription());
    }

    @Test
    void setDescription() {
        event1.setDescription("Local party in Eindhoven");
        assertEquals("Local party in Eindhoven", event1.getDescription());
    }

    @Test
    void getId() {
        assertEquals(1l, event1.getId());
    }

    @Test
    void setId() {
        event1.setId(2l);
        assertEquals(2l, event1.getId());
    }

    @Test
    void getDate() {
        assertEquals("2021-01-15", event1.getDate());
    }

    @Test
    void setDate() {
        event1.setDate("2021-01-16");
        assertEquals("2021-01-16", event1.getDate());
    }
}