package razvan.sem3.project.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import razvan.sem3.project.resources.UserDTO;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class VenueTest {

    private Venue venue1;
    private Event event1;
    private User user1;

    @BeforeEach
    void setUp(){
        user1 = new User("user1", "1234", "user1@gmail.com", "Razvan", "Simion");
        venue1 = new Venue("Shelter", "Frederiklaan", 150);
        event1 = new Event("Party", 2l, "Best Party", "2021-01-15", "22:00:00");
        venue1.setId(2l);
    }

    @Test
    void setId() {
        venue1.setId(1l);
        assertEquals(1l, venue1.getId());
    }
    @Test
    void Role(){
        Role role = new Role();
        assertNotNull(role);
    }
    @Test
    void getId() {
        assertEquals(2l, venue1.getId());
    }

    @Test
    void getName() {
        assertEquals("Shelter", venue1.getName());
    }

    @Test
    void setName() {
        venue1.setName("Guesthouse");
        assertEquals("Guesthouse", venue1.getName());
    }

    @Test
    void getLocation() {
        assertEquals("Frederiklaan", venue1.getLocation());
    }

    @Test
    void setLocation() {
        venue1.setLocation("Popa nan");
        assertEquals("Popa nan", venue1.getLocation());
    }

    @Test
    void getCapacity() {
        assertEquals(150, venue1.getCapacity());
    }

    @Test
    void setCapacity() {
        venue1.setCapacity(350);
        assertEquals(350, venue1.getCapacity());
    }

    @Test
    void testToString() {
        String text = "Venue{" +
                "name='" + venue1.getName() + '\'' +
                ", location='" + venue1.getLocation() + '\'' +
                ", capacity=" + venue1.getCapacity() +
                '}';
        assertEquals(text, venue1.toString());
    }

}