package razvan.sem3.project.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoleTest {

    private Role role1;

    @BeforeEach
    void setUp(){
        role1 = new Role("ADMIN");
        role1.setId(1l);
    }
    @Test
    void getId() {
        assertEquals(1l, role1.getId());
    }

    @Test
    void setId() {
        role1.setId(2l);
        assertEquals(2l, role1.getId());
    }

    @Test
    void getName() {
        assertEquals("ADMIN", role1.getName());
    }

    @Test
    void setName() {
        role1.setName("ARTIST");
        assertEquals("ARTIST", role1.getName());
    }
}